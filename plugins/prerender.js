const fs = require('fs');
const path = require('path');
const express = require('express');
const applyHbs = require('../server/hbs');
const options = require('../server/settings');

const app = express();
applyHbs(app);

class HBSPrerenderPlugin {
  apply(compiler) {
    compiler.hooks.done.tap('HBS Prerender plugin', () => {
      fs.readdir(app.get('views'), function(err, files) {
        if (err) {
          return console.log('Unable to scan directory: ' + err);
        }
        files.forEach(function(file) {
          if (file.includes('.hbs'))
            app.render(file, { resourcesUrl: options.config.resourcesUrl, ...options }, function(err, html) {
              fs.writeFile(path.join(__dirname, '../dist', file.replace('hbs', 'html')), html, function(err) {
                if (err) {
                  return console.log(err);
                }
                // console.info(`🛠 Template file ${file} have been prerendered`);
              });
            });
        });
      });
    });
  }
}

module.exports = HBSPrerenderPlugin;
