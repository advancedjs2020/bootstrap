import Vue from 'vue';

export async function loadApp(bundleName) {
  return await System.import(`./${bundleName}.js`)
    .then(result => {
      return result.default;
    })
    .catch(error => {
      console.info(`Загрузить ${bundleName} не удалось!`);
      console.info(`error in ${bundleName}`, error);
    });
}


const bootstrap = Vue.component('button-counter', {
  methods: {
    async downloader(bundleName: string) {
      let _bundleName = bundleName;
      this.currentBundle = bundleName;
      this.App = await loadApp(_bundleName);

    },

    async mountApp(nameApp) {
      await this.downloader(nameApp);
      const appDiv = document.createElement("div");
      appDiv.id = nameApp;
      this.mountPoint = appDiv;
      document.getElementById('app').appendChild(appDiv);
      this.App.mount(this.App.default, appDiv)
    },

    unmountApp() {
      console.log(this.App.default);
      this.App.default.$destroy();
      document.getElementById('app').innerHTML = '';
    },
  },
  data: function() {
    return {
      App: null,
      mountPoint: null,
      currentBundle: ''
    };
  },
  template: `
      <div>
          <button @click="mountApp('example')">
          загрузить приложение 1
          </button>
          <button @click="unmountApp()">
          выгрузить приложение 1
          </button>
          <button @click="mountApp('transport')">
          загрузить приложение 2
          </button>
          <button @click="unmountApp()">
          выгрузить приложение 2
          </button>
      </div>
  `
});

new bootstrap().$mount('#bootstrap');
