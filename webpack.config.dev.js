const path = require('path');
const webpack = require('webpack');
const webpackCopy = require('copy-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const { webpack: lernaAliases } = require('lerna-alias');

const outputDirectory = 'dist';

module.exports = {
  mode: 'development',
  entry: {
    bootstrap: ['webpack-hot-middleware/client?reload=true&path=/__webpack_hmr&timeout=20000', './src/main.ts'],
    example: '../../apps/example/main.ts',
    transport: '../../apps/jsmediapult/src/main.tsx',
  },
  output: {
    // publicPath: '/static/',
    publicPath: '/',
    filename: '[name].js',
    path: path.resolve(__dirname, outputDirectory),
    libraryTarget: 'umd',
    globalObject: `(typeof self !== 'undefined' ? self : this)`
  },
  node: {
    fs: 'empty'
  },
  plugins: [
    // new CleanWebpackPlugin(),
    new webpack.ProgressPlugin(),
    new webpack.DefinePlugin({
      'typeof window': JSON.stringify('object')
    }),
    new webpackCopy([
      { from: 'public/importmap.json' },
      { from: '../../node_modules/systemjs/dist', to: 'extlib/systemjs' },
      { from: '../../node_modules/vue/dist/vue.min.js', to: 'extlib/vue/vue.min.js' },
      { from: '../../node_modules/vuex/dist/vuex.min.js', to: 'extlib/vuex/vuex.min.js' },
      { from: '../../node_modules/vue-router/dist/vue-router.min.js', to: 'extlib/vue-router/vue-router.min.js' }
      // { from: 'exampleApp' }
    ]),
    new webpack.HotModuleReplacementPlugin(),
    new VueLoaderPlugin()
  ],
  devtool: 'source-map',
  resolve: {
    modules: ['node_modules', 'src'],
    extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js', '.css'],
    alias: lernaAliases()
  },

  module: {
    rules: [
      { parser: { system: false } },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.tsx?$/,
        loader: 'awesome-typescript-loader'
      },
      {
        test: /\.(jpe?g|gif|png|svg|woff|ttf|eot|wav|mp3)$/,
        loader: 'file-loader'
      }
    ]
  },
  externals: {
    vue: 'vue',
    vuex: 'vuex',
    'vue-router': 'vue-router'
  }
};
