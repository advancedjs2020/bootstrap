const config = require('./config.json');
const launcher = require('./launcher.json');
const navigation = require('./navigation.json');

module.exports = {
  config,
  launcher,
  navigation
};
