const express = require('express');
const applyHbs = require('./hbs');
const webpack = require('webpack');
const devMiddleware = require('webpack-dev-middleware');
const hotMiddleware = require('webpack-hot-middleware');
const config = require('../webpack.config.dev');
const options = require('./settings');
const { publicPath } = config.output;

const app = express();

applyHbs(app);

const compiler = webpack(config);
app.use(devMiddleware(compiler, { publicPath }));
app.use(hotMiddleware(compiler));

app.use(['/'], (request, response) => {
  response.render('index.hbs', { resourcesUrl: options.config.resourcesUrl, ...options });
});

app.listen(8090, () => console.log('🛠', 'Bootstrap is available on the port:', 8090));


module.exports = app;
